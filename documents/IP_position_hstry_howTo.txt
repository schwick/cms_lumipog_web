W. Kozanecki								     Last revision: 7 May 17





        Instructions for extracting orbit fit data from an LHCop console
        ================================================================



1. Setup optics model & orbit fits
   _______________________________


- Fron an LHC console (e.g. the one in the middle of the LHC island in CCC):
  Favorites -> LHC steering

- LHC steering -> Optics & Model
			-> Optics configs & details
                  	-> Selection
                           (click on optics file [1]) -> Load 
				   (this makes it active)

               -> Optics & Models -> Fits 
                  * tick 'Show' on the relevant 4 fit panels [2]
                    This opens 4 windows for extrapolations of B1L, B1R, B2L, B2R orbits.
                  * VERY IMPORTANT: hit "Update Optics" on the "Betatron fits" panel (bottom
		    right corner) so that the fits are excuted with the optics file you just 
                    loaded. Check that the 4 fit windows display the name of the optics file 
		    you loaded above. If you skip this step, the results may be nonsensical. 

- decide whether you want to:
  2a: acquire live data
  2b: replay data from database



2a. Acquire live data
    _________________

- on steering panel:  -> tick Difference

- -> file menu -> MD data set catalog 
			-> Add [a new orbit]; edit name (new Entry -> vdM ref N)
			-> Load as reference (do it twice!)
                           (this loads this recent orbit as the steering ref)

- on steering panel: slide mouse to Difference, check it uses the orbit you want

- on steering panel: to start acquiring, hit the big green & white arrow. 
			   Make sure that the diff orbit is reasonable; 
			   otherwise you may have to relaod the diff  

- on steering panel: to stop acquiring, hit the white & red stop button             
   



2b. Retrieve orbit data from database 
    _________________________________

               -> On each of the 4 orbit-fit displays, "Fit Evolution" tab: 
                  * change the downsample factor from 5 to 1 (do this ONLY 
		     when retrieving from the DB, not when acquiring live data)

               -> Machine specials
                  -> Multiple acquisitions (this panel takes a while to open)
                     -> Control/Display --> Del. All (this clears the orbit buffer of any
                                                      previously acquired orbits)
                     -> Logging -> By time (enter sampling period ('every…'),
                                            start & stop times,
					     make sure to hit CR in each time box) 
                                   Note that the archiving of the results is limited
                                   to 600 samples: the combination of start time, stop time
                                   and sampling period must be chosen accordingly.  
                                -> OK (this starts data extraction)
                                   When extraction complete, click on 1st orbit:
                                   This will define the reference orbit.
                    -> Data sets -> Active dataset (in Data Set List) -> >> Ref
                                 -> tick Difference

		-> On each of the 4 orbit-fit displays, "Fit Evolution" tab: 
                   hit "Reset plots" to clear any data from the fit-results buffers.
		   It is IMPERATIVE to do this just before the Replay below. The reason is that 
                   during the extraction above, the process pushes the extracted 
		   orbit to the display every time it gets a new orbit from the DB. These need 
		   to be cleared before acccumulating the orbit-difference results.

                 -> Data Collection LHCRING -> Replay 
						-> Delay per step 600 ms (not less than 500, 
						   otherwise potential GUI problems!)
						-> hit big green/white arrow:
						   this starts the processing,
                                                   which plays back the orbit history



3. Save data and transfer them to lxplus
   _____________________________________


Open a 1st xterm (right-mouse):
  $ cd
  $ mkdir myDirectoryName [or $ cd whateverDirectoryNameYouCreated]
   (suggested names are something like ALICE_MG, ATLAS_WK, CMS_MZ, etc)
    [from here, you can watch that the files you want to save did end up
     at the right place]


For each of the 4 open plots, switch to the 'Target params' tab, click on 'Export Pos' 
and navigate to your local (lhcop/ATL… or lhcop/CMS…) directory.
Repeat for the 4 angle plots ("Export Angle").




Open a 2nd xterm:

  $ kinit witold [or whatever your afs username is]

  $ aklog
    [this gives access to your lxplus area as a subdirecctory of ~lhcop]

  $ cd /afs/cern.ch/user/w/witold/WTT/LHC_transit  
    [or wherever you want to store them on lxplus]

  $ cp ~/ATLAS_WK/myFile   .
    [this copies the file myFile to ~witold/WTT/LHC_transit]







4. Mopping up
   ----------

   When you are done, please exit the steering package by closing the main 
application window (cross in top right corner of panel). Also close you x-terms.

        



Notes
=====


[1] Optics models
    -------------

	vdM scan	 	b* @ IP1+5 / E_beam			Model name

  	Mar 2011			11.0/1.38		A1100C1100A1000L1000_INJ_2011
  	May 2011		 	 1.5/3.5			A150C150A1000L300_0.00875_2011
   	October 2011        	 1.0/3.5			A100C100A1000L300_0.00875_2011
    	Nov 2011 (HI)		 1.0/3.5Z		A100C100A100_0.00889L300_0.00875_2011
      	Apr 2012	 		 0.6/4			A60C60A300_0.00889L300_0.00875_2012
	Jul & Nov 2012		11.0/4			A1100C1100A1000L1000_2012
	Jan 2013 p-Pb/Pb-p  	 0.8/4Z			A80C80A80_0.00889L200_0.00872_2012
	Feb 2013 low-E pp   	11.0/1.38		A1100C1100A1000L1000_2012
	Aug 2015, pp		19.2/6.5			R2015c_A19mC19mA19mL24
	Nov 2015, pp		 4.0/2.51		R2015a_A400C400A10m_0.00950L700_0.00906
	Dec 2015, PbPb	         0.8/6.37Z		R2015i_A80C80A80L300
	May 2016, pp		19.2/6.5		  	R2016c_A19mC19mA19mL24m
	Sep 2016, pp		 0.4/6.5			R2016a_A40C40A10mL300
	Nov 2016, pPb+Pbp	 0.6/6.5	Z		R2016i2_A60C60A200L150		



[2] Fits
    ----

For IP1: From/To	BPM.33L1.B1 / BPM.9L1.B1
			BPM.9R1.B1  / BPM.33R1.B1
			BPM.33L1.B2 / BPM.9L1.B2
			BPM.9R1.B2  / BPM.33R1.B2

For IP2: From/To	BPM.33L2.B1 / BPM.9L2.B1
			BPM.9R2.B1  / BPM.33R2.B1
			BPM.33L2.B2 / BPM.9L2.B2
			BPM.9R2.B2  / BPM.33R2.B2
                
For IP5: From/To	BPM.33L5.B1 / BPM.9L5.B1
			BPM.9R5.B1  / BPM.33R5.B1
			BPM.33L5.B2 / BPM.9L5.B2
			BPM.9R5.B2  / BPM.33R5.B2                               

For IP8: From/To	BPM.33L8.B1 / BPM.9L8.B1
			BPM.9R8.B1  / BPM.33R8.B1
			BPM.33L8.B2 / BPM.9L8.B2
			BPM.9R8.B2  / BPM.33R8.B2
