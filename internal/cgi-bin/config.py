#!/usr/bin/python3

import json
import os

def getConfig():
    configfile = "../data/config.json"
    fd = open(configfile,'r')
    config = json.load( fd )
    fd.close()
    config["SITEURL"] = config["SERVERURL"] + config["SITEBASE"]
    return config
