#!/usr/bin/python3
import json
import cgi
import os
import glob
import cgitb
import re
import datetime
import shutil
from checkLogin import checkCookies

cgitb.enable()

checkCookies("/git-source/internal/editMinutes.py")


if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))

pp = os.path.join( pp, ".." )

form = cgi.FieldStorage()

# globals

pdir = os.path.join( pp, "minutes-workdir")
mdir = os.path.join( pp, "minutes" )
pf = os.path.join( pdir, "currentMinutes.json" )

action = form['action'].value
error = ""
debug = ""

def loadsummaryjson( filename = "currentMinutes.json" ):
    psf = os.path.join( pdir, filename )
    fd = open( psf, "r" )
    res = json.load( fd )
    # check if published already, if so provide link
    (htmlname,ext) = os.path.splitext( filename )
    htmlname += ".html"
    pubpath = os.path.join( mdir, htmlname )
    if os.path.isfile( pubpath ):
        link = "../../minutes/" + htmlname
    else:
        link = ""
    res['link'] = link
    fd.close()
    return res

def formatDate( date, reverse=False ):
    dat = datetime.datetime.strptime( date, "%Y-%m-%d" )
    if ( reverse ):
        return datetime.datetime.strftime( dat, "%Y-%m-%d" )
    return datetime.datetime.strftime( dat, "%d-%m-%Y" )

def saveSummary() :
    text = form['text'].value
    date = form['date'].value
    if "indicourl" in form:
        indicourl = form['indicourl'].value
    else:
        indicourl = ""
    shortdesc = form['shortdesc'].value
    purpose = form['purpose'].value
    if 'suffix' in form:
        suffix = form['suffix'].value
    else:
        suffix = ""
    datarecord = { 'text' : text,
                   'date' : date,
                   'suffix' : suffix,
                   'indicourl' : indicourl,
                   'shortdesc' : shortdesc,
                   'purpose' : purpose }
    fd = open( pf, "w" )
    json.dump( datarecord, fd )
    fd.close()
    # also save under the official name
    if suffix != "" :
        suffix = "_" + suffix
    nfn = os.path.join( pdir, formatDate( date, True ) + suffix + ".json" )
    nfd = open( nfn, "w" )
    json.dump( datarecord, nfd )
    nfd.close()


if action == 'save':
    saveSummary();
    res = { "status" : "saved" }

elif action == 'load':
    res = loadsummaryjson()
    res["status"] =  "loaded"

elif action == 'loadPrevious':
    filename = form['summaryName'].value
    res = loadsummaryjson( filename )
    #res = loadpreviousminutes()
    res["status"] =  "loaded"

elif action == 'addToList':
    listpage = os.path.join( pp, "lumi-meetings.html" )
    fd = open( listpage, 'r' )
    page = fd.read()
    fd.close()
    #mo = re.match('(.+id=\"listbody\">)(.+)', page);
    mo = re.match('(.+id="listbody">)(.*)', page, re.DOTALL);
    status = "no action done"
    if mo:
        pre = mo.group(1)
        post = mo.group(2)
        newpage = pre
        newpage += '\n<tr>\n<td>\n' + form['date'].value + '</td>\n'
        newpage += '<td>' + form['shortdesc'].value + '</td>\n'
        suffix = ''
        if 'suffix' in form:
            suffix = "_" + form['suffic'].value
        newpage += '<td><a href="minutes/' + form['date'].value + suffix + '.html"><b>DRAFT</b> Summary</a></td>\n'
        link = ""
        if 'indicourl' in form:
            link = '<a href="' + form['indicourl'].value + '">Indico</a>'
        newpage += '<td>' + link + '</td>\n'
        newpage += '</tr>\n'
        newpage += post

        # need to write back the page here.
        status = "added to list"
        fd = open( listpage, 'w' )
        page = fd.write( newpage )
        fd.close()
    res = {'status' : status }

elif action == 'release':
    cm = loadsummaryjson()
    suffix = cm['suffix']
    if suffix != "" :
        suffix = "_" + suffix

    newFilename = formatDate( cm['date'], True ) + suffix + ".html"    
    nfp = os.path.join( mdir, newFilename )
    if not os.path.isfile( nfp ):
        error = "The summary " + newFilename + "does not exist! You cannot release it!"
        res = { 'status' : "problem" }
    dest = os.path.join( pp, "..", "minutes" )
    shutil.copy( nfp, dest)
    res = { 'status' : 'released' }
           
        
elif action == 'publishNew' or action == 'publishUpdate':
    # read in template
    tfn = os.path.join( pp, "minutes")
    tfn = os.path.join( tfn, "template.html" )
    tfd = open( tfn, "r")
    template = tfd.read()
    tfd.close()
    # read the current minutes dictionary
    cm = loadsummaryjson()
    # replace date
    datstr = formatDate( cm['date'] )
    template = re.sub( r'__date__', datstr, template, re.MULTILINE ) 
    # replace indico frame
    template = re.sub( r'__indicourl__', cm['indicourl'], template, re.MULTILINE )
    # replace purpose
    template = re.sub( r'__purpose__', cm['purpose'], template, re.MULTILINE )
    # insert text
    template = re.sub( r'__text__', cm['text'], template, re.MULTILINE )
    
    suffix = cm['suffix']
    if suffix != "" :
        suffix = "_" + suffix

    newFilename = formatDate( cm['date'], True ) + suffix + ".html"    
    nfp = os.path.join( mdir, newFilename )
    if action == 'publishNew' and os.path.isfile( nfp ):
            error = "The summary " + newFilename + " exist already!"
            res = { 'status' : "problem" }
               
    elif action == "publishUpdate" and not os.path.isfile( nfp ):
        error = "The summary " + newFilename + " does not exist! You cannot update it!"
        res = { 'status' : "problem" }
           
    else:
        nfd = open( nfp, "w" )
        nfd.write( template )
        debug = template[1200:1220]
        nfd.close()
        link = "../../minutes/" + newFilename
        res = { "status" : "published",
                "link" : link } 
           
else:
    error =  "action " + action + " not known"
    res = { "status" : "problem" }
    
res['error'] = error
res['debug'] = debug
print ("Content-type: application/json")
print ("")
print (json.dumps( res ))

