#!/usr/bin/python3
from datetime import datetime
import os
import time

class Lock:
        def __init__( self, name, path, logfile ):
            self.name = name
            self.path = path
            self.takenlock = os.path.join( path, name + "_taken" )
            self.freelock  = os.path.join( path, name + "_free" )
            self.logfile = logfile
            if not os.path.isfile( logfile ):
                self.log( "created logfile" )
            if (not os.path.isdir( self.freelock )) and (not os.path.isdir( self.takenlock )):
                os.makedirs( self.freelock )

            if os.path.isdir( self.freelock ):
                self.status = "free"
            else:
                self.status = "taken"
                
        def takelock( self ):
            sleeptime = 0
            deltaloop = 0.5
            timeout = 5.001
            if self.status <> "free" :
                self.log( "software bug: cannot take a lock which I have already taken. Lock name : " + self.name )
                return False

            while os.path.isdir( self.takenlock ) and sleeptime <= timeout:
                self.log( "lock is taken since " + str(sleeptime) + " seconds")
                time.sleep( deltaloop )
                sleeptime += deltaloop
            if sleeptime > timeout:
                self.log( "timeout while waiting for lock " + self.name )
                return False

            try:
                os.rename( self.freelock, self.takenlock )
                self.status = "taken"
            except OSError as e:
                self.log("Could not rename " + self.freelock + " to " + self.takenlock )
                return False
            
            self.log( "Successfully took lock " + self.name )
            return True

        def givelock( self ):
            if self.status <> "taken" :
                self.log( "software bug: cannot free a lock which is not take by me. Lock name : " + self.name )
                return False

            if os.path.isdir( self.freelock ):
                self.log( "Software bug: cannot free a lock which is free. Lock name : " + self.name )
                return False

            try:
                os.rename( self.takenlock, self.freelock )
                self.status = "free"
            except OSError as e:
                self.log( "Could not rename " + self.takenlock + " to " + self.freelock )
                return False

            self.log ("Successfully freed lock " + self.name )
            return True

        def log( self, message ):
            fd = open( self.logfile, 'a' )
            now = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
            fd.write( now + " : " + message + "\n" )
            fd.close()

