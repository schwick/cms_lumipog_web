#!/usr/bin/python3

import cgitb
import cgi
import os
import re
import sys
import json
import glob
from checkLogin import checkCookies

cgitb.enable()
checkCookies()


if "PATH_TRANSLATED" in os.environ:
    pp = os.path.dirname(os.path.dirname(os.environ["PATH_TRANSLATED"]))
else:
    pp = os.path.dirname(os.path.dirname(os.environ["SCRIPT_FILENAME"]))


# get the list of previously edited minutes
pdir = os.path.join( pp, "..", "minutes-workdir")
jsnames = os.path.join( pdir, "*.json")
jsfiles = glob.glob( jsnames )
nfiles = []
for fn in sorted(jsfiles):
    nfiles.append(os.path.basename(fn))
htmlout = '''Content-Type: text/html

<!DOCTYPE HTML>
<html>
<head>
  <title>Edit Lumi Meeting Summaries</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="../../css/site.css">
  <link rel="stylesheet" type="text/css" href="../../css/jquery-ui.css">
  <script src="../../js/jquery-2.2.0.min.js"></script>
  <script src="../../js/jquery-ui.min.js"></script>
  <script src="../../js/ckeditor/ckeditor.js"></script>
  <script src="../../js/utils.js"></script>
  <script src="../../js/editMinutes.js"></script>
</head>
<body onload="newSummary()">
    <!-- page header -->

    <table id="table_header" style="width:100%">
      <tr>
	<td style="width:72px">
	  <a href="http://user.web.cern.ch/user/Welcome.asp">
	    <img src="../../images/CERNlogo.gif" alt="CERN" width="72" height="72">
	  </a>
	</td>
	<td>
	  <p class="header-headline">
	    Edit Lumi Minutes
	  </p>
	  <p class="center">
	    <a href="../../index.html">LumiPOG home</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="../index.html">Internal pages</a>
	  </p>
	</td>
	<td style="width:72px">
	  <img alt="LPC: 79977" src="../../images/lpcnum.gif" width="72" height="72">
	</td>
      </tr>
    </table>

    <hr>
    
    <!-- page header end -->

    <div id="error"></div>
    <div id="warning"></div>
    <div id="debug"></div>
    <div id="contents">
'''

formhtml = '''
<form name="editMeeting" id="editMeeting">
<div style="display: inline-block">
<h2>Meeting info</h2>
<table>
<tr><td><label>Date of the meeting*: </label></td><td><input name="date" id="date" type="date" /></td></tr>
<tr><td><label>Filename suffix: </label></td><td><input name="suffix" id="suffix" type="text" /></td></tr>
<tr><td><label>URL of the indico agenda: </label></td><td><input name="indicourl" id="indicourl" type="url" /></td></tr>
<tr><td><label>Short description of the meeting*: </label> </td><td><input name="shortdesc" id="shortdesc" type="text" /></td></tr>
<tr><td><label>Purpose of the meeting*:</label></td><td><textarea name="purpose" id="purpose"></textarea></td></tr>
</table>
<span class="small">* Essential fields which must be filled before the document will be saved.</span>
</div>
<div style="display:inline-block">
<h2 style="margin-left: 20px;">Editor actions</h2>
<table style="margin-left:20px">
<tr><td><label>Create new summary: </label></td><td><input type="button" value="New summary" onclick="newSummary()"></td></tr>
<tr><td><label>Load current summary: </label></td><td><input type="button" value="Load current" onclick="loadSummary()"></td></tr>
<tr><td><label>Load previous summary:</label></td><td><select id="summaryName" onchange="loadPreviousSummary()">
<option label=\" \" value="" selected></option>
'''
for f in nfiles:
    formhtml += '<option value="' + f + '">' + f + '</option>'
formhtml += '''</select></tr>
<tr><td></td><td>&nbsp;</td></tr>
<tr><td></td><td>&nbsp;</td></tr>
<tr><td><label>Publish new summary in development area: </label></td><td><input type="button" value="Publish NEW" onclick="publishSummary( 'publishNew' )"></td></tr>
<tr><td></td><td>&nbsp;</td></tr>
<tr><td><label>Update existing summary in development area: </label></td><td><input type="button" value="Update EXISTING" onclick="publishSummary( 'publishUpdate' )"></td></tr>
<tr><td><label>Copy summary to production site: </label></td><td><input type="button" value="Release" onclick="release()"></td></tr>
<tr><td colspan="2" id="devlink"></td></tr>
<tr><td><label> Create a new entry in the Overview Table: </label></td><td><input type="button" value="Add to Overview" onclick="addToList()"></td></tr>
<tr><td colspan="2" id="devlink"></td></tr>
</table>
</div>
</form>
<div id="status"></div>
'''


htmlout += formhtml
htmlout += '''
<div id="pagetext" contenteditable="true" class="minutes-text">
</div>
</div>
</body>
</html>
'''
print (htmlout)

