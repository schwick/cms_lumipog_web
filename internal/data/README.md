The file _config.json is the config file used in production. It serves as an example.
In order to get this website up and running on any arbitrary server the values in this
file need to be edited adequately:
- SERVERURL is the url of the server, how it is accessed from the internet.
- SITEBASE is the part of the url after the SERVERURL which leads to the root of the website.
         it ends with "git-source" since this website is designed to have a development site
         under git-source (and the files here are tracked in git) and a production site which
         is found just one level up in the directory hierarchy. SITEBASE points to the root 
         of the development site.
- FSROOT   Is the full path in the filesystem of the server to the root of the development site
         (which is under git).


