# Variables before the horizontal line need to be edited


# The top directory of the website in the servers file-system
filesysroot = "/home/schwick/public_html/lumipog"

# identifies the server of the host
serverurl = "http://localhost"

# serverbase is the part of the url after the server name which leads to the
#            root of the website
serverbase = "~schwick/lumipog"

# baseurl is the url to the top level of the web site (where the production
#         version pages are)
baseurl = serverurl + serverbase


#-------------------------------- no edit necessary beyond this line -------------------------------

internalroot = baserurl + "/git-source/internal"


