# Create a docker image for the web site

In this directory you can create a new docker image and upload it to the
openshift cluster so that it will be used for the website.
The commands to execute can be found on the gitlab web page of the website:
On the left navigation bar choose Packages & Registries --> Container Registry
On the top right you find a button CLI Commands which shows three commands
which have to be exectued in sequence:

docker login gitlab-registry.cern.ch
docker build -t gitlab-registry.cern.ch/schwick/cms_lumipog_web .
docker push gitlab-registry.cern.ch/schwick/cms_lumipog_web

The last of these pushes the image to the docker registry in gitlab. It should
be picked up automatically by the openshift cluster.

