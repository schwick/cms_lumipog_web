#!/bin/bash
cd /tmp
nohup fcgiwrap -s unix:/var/run/fcgiwrap.socket&
chmod a+w /var/run/fcgiwrap.socket
nginx -g "daemon off;"
