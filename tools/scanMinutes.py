#!/usr/bin/python
import os
import glob
import re
import pprint
import json

mindir = "../lpc-minutes"
tabfile = "../lpc-meetings.htm"

fd = open( tabfile, "r")
tabhtml = fd.read()
fd.close

files = glob.glob( mindir + "/*.htm")
summaries = {}
for m in files :
    bn = os.path.basename( m )
    if bn == "template.htm":
        continue
    #print bn
    fd = open( m, "r")
    html = fd.read()
    fd.close()

    # analyse filename for data and suffix:
    mo = re.match( r'((\d\d\d\d)-(\d\d)-(\d\d))_?(.+)?\.htm', bn )
    if mo:
        date = mo.group(1)
        suffix = mo.group(5)
        if suffix == None:
            suffix = ""
        year = int(mo.group(2))
        month = int(mo.group(3))
        day = int(mo.group(4))
            
    else:
        print "Problem with "+bn+" the pattern did not match"
        continue

    mo = re.search(r'class="indico-iframe" src="([^"]+)"', html)
    if mo:
        indicourl = mo.group(1)
    else:
        print "Problem with "+bn+": cannot find indico url"
        indicourl = ""
        
    #mo = re.search(r'<strong>Main purpose of the meeting: </strong>\s*(.+?)\s*</p>', html)
    mo = re.search(r'<strong>Main purpose of the meeting:\s?</strong>(.+?)</p>\s*(.+?)\s*</div>', html, re.DOTALL)
    if mo:
        purpose = mo.group(1)
        purpose = purpose.strip()
        text = mo.group(2)
        text = text.strip()
    else:
        print "Problem with "+bn+": cannot find purpose of the meeting."
        purpose = ""
        text = ""

    # Now find short description
    shortdesc = ""
    for mo in re.finditer( r'(\d\d?)-(\d\d?)-(\d\d\d\d).+?<td>\s*(.+?)\s*</td>', tabhtml, re.DOTALL ):
        dday = int(mo.group(1))
        mmonth = int(mo.group(2))
        yyear = int(mo.group(3))
        shortdesc = mo.group(4)
        if ( year == yyear and day == dday and month == mmonth ):
            break
    if shortdesc == "":
        print "Problem with " + bn + ": cannot find short description"
    
    summaries[bn] = { 'date' : date,
                      'suffix' : suffix,
                      'indicourl' : indicourl,
                      'shortdesc' : shortdesc,
                      'purpose' : purpose,
                      'text': text}
    
#pprint.pprint( summaries )

for filename,val in summaries.iteritems():
    (filename, ext) = os.path.splitext( filename )
    filename = "tmp/" + filename + ".json"
    print filename
    fd = open( filename, "w")
    json.dump( val, fd )
    fd.close()
    
