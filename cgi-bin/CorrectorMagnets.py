#!/usr/bin/python3
import cgi
import cgitb
import os
import json
import re
import sys
import json

cgitb.enable()

pp = os.path.dirname(os.environ["SCRIPT_FILENAME"])

form = cgi.FieldStorage()

def getplot( fn ):
    fd = open(fn,'r')
    div = fd.read()
    fd.close()
    return div

def getInfo():
    filename = "../data/Correctors/plots/info.json"
    fd = open( filename, "r")
    info = json.load( fd )
    fd.close()
    return info

def ajaxplots(fill):
    filenamebase = "../data/Correctors/plots/"
    filename = os.path.join( filenamebase, fill + "_IP5_currents.div" )
    filename_norm = os.path.join( filenamebase, fill + "_IP5_currents_norm.div" )
    filename_pict = os.path.join( filenamebase, fill + "_IP5_picture.div" )
    info = getInfo()
    message = "no remarks for this fill"
    if fill in info:
        message = info[fill]
    res = { 'current' : getplot( filename ),
            'current_norm' : getplot( filename_norm ),
            'picture' : getplot( filename_pict ),
            'message' : "<strong>Info for fill " + fill + " : </strong>" + message
            }
    print ("Content-type: application/json")
    print ("")
    print (json.dumps( res ))

    return

# Check if this is an Ajax request to get the plot data
if "fillno" in form:
    fill = form['fillno'].value
    ajaxplots( fill )
    sys.exit()

pagehtml = '''<!DOCTYPE HTML >
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <title>CMS Lumi POG</title>
    <link rel="stylesheet" type="text/css" href="../css/site.css">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/plotly.min.js"></script>
    <script src="../js/drawCorrectorMagnets.js"></script>
    <script src="../js/dorosPlots.js"></script>
    <script src="../js/tab.js"></script>
  </head>

  <body onload="dotab()"><!-- page header -->
<table style="width:100%">
	<tbody>
		<tr>
			<td style="width: 72px; text-align: justify;"><img alt="CERN" height="72" src="../images/CMS-Color-Label.gif" width="72" /></td>
			<td>
			<p class="header-headline">Corrector Magnets for VdM scans</p>

			<p class="center"><a href="../index.html">LumiPOG home</a></p>
			</td>
			<td style="width: 72px;"><img height="72" src="../images/LumiPOG.gif" width="72" /></td>
		</tr>
	</tbody>
</table>

<hr />

<script>
  function dotab() {
  console.log( "dotab" );
  var tabber = new Tabber( "pageContent" );
  tabber.register("Corrector Magnets", "tab1");
  tabber.register("Corrector Magnet Current Plots", "tab2");
  tabber.render();
}
</script>

<div id="pageContent">
<div id="tab1">
<h2>Closed Bumps for vdM scans</h2>

<p>Four orbit correctors are necessary&nbsp;per beam and per plane in order&nbsp;to create the closed orbit Bump for&nbsp;the vdM scans (closed means that the orbit bumps are not visible outside of this region).&nbsp;The Knob factor gives the conversion factor from [mm] to [rad] deflection angle for the 19m optics. Knobs in the LHC are algorithms which contain the logic to steer multiple corrector magnets in concertation in order to obtain a desired effect. In the vdM scans closed obrit Bumps should be created by changing four corrector magnets in a synchronised way. As the figure below shows, Knobs are designed to propagate the desired effect through a hierarchy of values. (The figure shows 5 magnets, however in vdM scans only 4 corrector magnets are involved.)</p>

<ol>
	<li>The desired effect is at the top of the hierarchy (in vdM scans this is the desired displacement of the beam in mm)</li>
	<li>For each involved corrector magnet a Knob Factor is used to convert the desired displacement to the required angular kick for the corrector magent. These factors are independent of the magnet calibration curves and the beam energy.&nbsp;</li>
	<li>The next level (third green row in the figure below) in the case of Stable Beams just contains a copy of the kick values in the second row.
	<li>The LSA database of the LHC then contains the conversion factors which convert the magnetic kick into an electrical current. These factors depend on the beam energy and the calibration curve of the individual magnet.&nbsp;</li>
	<li>Finally the resulting currenting currents are requersted from the involved Power Converters.&nbsp;<img alt="" src="../images/KnobToPowerConverter.png" style="width: 100%;" /></li>
</ol>


<h2>Table of involved corrector magnets in IP5</h2>

<p>Looking from the center of the LHC to the IP, negative distance values are found in counter clockwise direction from the IP (for IP5 this means closer to the Jura, in CMS called &quot;left side&quot;), and positive values are closer to the airport (&quot;right side&quot;).</p>

<table border="1" cellpadding="1" cellspacing="1" class="clearAndSimple">
	<thead>
		<tr>
			<th scope="col">Beam Plane</th>
			<th scope="col">Magnet Name</th>
			<th scope="col">Dist. IP5</th>
			<th scope="col">Logical Hw Parameter Name</th>
			<th scope="col">Timber Variable</th>
			<th scope="col">Knob Factor</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>B1 X</td>
			<td>MCBYH.4R5.B1:MCBYH</td>
			<td>165.735</td>
			<td>RCBYHS4.R5B1/KICK</td>
			<td>RPLB.RR57.RCBYHS4.R5B1:I_MEAS</td>
			<td>1.182465668E-4</td>
		</tr>
		<tr>
			<td>B1 X</td>
			<td>MCBCH.5R5.B1:MCBCH</td>
			<td>&nbsp;199.532</td>
			<td>RCBCH5.R5B1/KICK</td>
			<td>RPLB.RR57.RCBCH5.R5B1:I_MEAS</td>
			<td>-7.046607228E-5</td>
		</tr>
		<tr>
			<td>B1 X</td>
			<td>MCBCH.6L5.B1:MCBCH</td>
			<td>-225.348</td>
			<td>RCBCH6.L5B1/KICK</td>
			<td>RPLB.RR53.RCBCH6.L5B1:I_MEAS</td>
			<td>-2.315118311E-5</td>
		</tr>
		<tr>
			<td>B1 X</td>
			<td>MCBYH.A4L5.B1:MCBYH<br />
			MCBYH.B4L5.B1:MCBYH</td>
			<td>-164.439<br />
			-167.031</td>
			<td>RCBYH4.L5B1/KICK</td>
			<td>RPLB.RR53.RCBYH4.L5B1:I_MEAS</td>
			<td>3.191608726E-5</td>
		</tr>
		<tr>
			<td>B1 Y</td>
			<td>MCBCV.5L5.B1:MCBCV</td>
			<td>-193.448</td>
			<td>RCBCV5.L5B1/KICK</td>
			<td>RPLB.RR53.RCBCV5.L5B1:I_MEAS</td>
			<td>-7.829142986E-5</td>
		</tr>
		<tr>
			<td>B1 Y</td>
			<td>MCBYV.4L5.B1:MCBYV</td>
			<td>-165.735</td>
			<td>RCBYVS4.L5B1/KICK</td>
			<td>RPLB.RR53.RCBYVS4.L5B1:I_MEAS</td>
			<td>1.379426378E-4</td>
		</tr>
		<tr>
			<td>B1 Y</td>
			<td>MCBYV.A4R5.B1:MCBYV<br />
			MCBYV.B4R5.B1:MCBYV</td>
			<td>164.439<br />
			167.031</td>
			<td>RCBYV4.R5B1/KICK</td>
			<td>RPLB.RR57.RCBYV4.R5B1:I_MEAS</td>
			<td>3.641854137E-5</td>
		</tr>
		<tr>
			<td>B1 Y</td>
			<td>MCBCV.6R5.B1:MCBCV</td>
			<td>231.432</td>
			<td>RCBCV6.R5B1/KICK</td>
			<td>RPLB.RR57.RCBCV6.R5B1:I_MEAS</td>
			<td>-2.192295445E-5</td>
		</tr>
		<tr>
			<td>B2 X</td>
			<td>MCBCH.6R5.B2:MCBCH</td>
			<td>231.432</td>
			<td>RCBCH6.R5B2/KICK</td>
			<td>RPLB.RR57.RCBCH6.R5B2:I_MEAS</td>
			<td>-2.152948777E-5</td>
		</tr>
		<tr>
			<td>B2 X</td>
			<td>MCBYH.A4R5.B2:MCBYH<br />
			MCBYH.B4R5.B2:MCBYH</td>
			<td>164.439<br />
			167.031</td>
			<td>RCBYH4.R5B2/KICK</td>
			<td>RPLB.RR57.RCBYH4.R5B2:I_MEAS</td>
			<td>3.08280403E-5</td>
		</tr>
		<tr>
			<td>B2 X</td>
			<td>MCBCH.5L5.B2:MCBCH</td>
			<td>-193.448</td>
			<td>RCBCH5.L5B2/KICK</td>
			<td>RPLB.RR53.RCBCH5.L5B2:I_MEAS</td>
			<td>-7.922466571E-5</td>
		</tr>
		<tr>
			<td>B2 X</td>
			<td>MCBYH.4L5.B2:MCBYH</td>
			<td>-165.735</td>
			<td>RCBYHS4.L5B2/KICK</td>
			<td>RPLB.RR53.RCBYHS4.L5B2:I_MEAS</td>
			<td>1.320020703E-4</td>
		</tr>
		<tr>
			<td>B2 Y</td>
			<td>MCBCV.6L5.B2:MCBCV</td>
			<td>-225.348</td>
			<td>RCBCV6.L5B2/KICK</td>
			<td>RPLB.RR53.RCBCV6.L5B2:I_MEAS</td>
			<td>-2.305503172E-5</td>
		</tr>
		<tr>
			<td>B2 Y</td>
			<td>MCBYV.4R5.B2:MCBYV</td>
			<td>165.735</td>
			<td>RCBYVS4.R5B2/KICK</td>
			<td>RPLB.RR57.RCBYVS4.R5B2:I_MEAS</td>
			<td>1.237882739E-4</td>
		</tr>
		<tr>
			<td>B2 Y</td>
			<td>MCBCV.5R5.B2:MCBCV</td>
			<td>199.532</td>
			<td>RCBCV5.R5B2/KICK</td>
			<td>RPLB.RR57.RCBCV5.R5B2:I_MEAS</td>
			<td>-7.001306632E-5</td>
		</tr>
		<tr>
			<td>B2 Y</td>
			<td>MCBYV.A4L5.B2:MCBYV<br />
			MCBYV.B4L5.B2:MCBYV</td>
			<td>-164.43<br />
			-167.031</td>
			<td>RCBYV4.L5B2/KICK</td>
			<td>RPLB.RR53.RCBYV4.L5B2:I_MEAS</td>
			<td>3.855548273E-5</td>
		</tr>
	</tbody>
</table>

<p>The following sketch shows the position of all magnets around IP5. The Magnets and their positions are extracted from the LHC geometry file used in MADX. The Magnets used in the vdM scans are shown in red. Other corrector magnets (MCxxx) are in green. Quadrupoles are shown in blue. All other magnets (eg. the separation dipoles but also others) are shown in black. The magnets encountered by Beam 2 are drawn in the top half and the magnets encountered by Beam 2 in the bottom half. Note that the magnets close to IP5 and before the separation dipole are drawn in both halves since they encounter both beams (e.g. the Triplets Q1,Q2,Q3). Also note that each beam for each plane encounters one pair of corrector magnets which is powered by the same Power converter (see table above). In addition to the magnets also the position of the DOROS BPMs are indicated since they are interesting for vdM scans.</p>
<H2>IP5 Layout</H2>
<canvas height="700" id="ir5layout" style="border:1px solid #d3d3d3;" width="1600"> Your browser does not support the HTML5 canvas tag.</canvas>
<H2>IP1 Layout</H2>
As can be seen, IP1 and IP5 layouts are perfectly symmetric by design.
<canvas height="700" id="ir1layout" style="border:1px solid #d3d3d3;" width="1600"> Your browser does not support the HTML5 canvas tag.</canvas>

<script>
  var c = document.getElementById("ir5layout");
  var ctx = c.getContext("2d");
  ctx.translate( 500, 350 );
  ctx.font = "10px Verdana";

  drawBeam( ctx, Beam2, 1)
  drawBeam( ctx, Beam1, -1)
  drawDist( ctx, "ip5" );

  // Now IP1

  var c = document.getElementById("ir1layout");
  var ctx = c.getContext("2d");
  ctx.translate( 500, 350 );
  ctx.font = "10px Verdana";
  
  drawBeam( ctx, IP1_Beam2, 1)
  drawBeam( ctx, IP1_Beam1, -1)
  drawDist( ctx, "ip1" );


  
</script></div>


<div id="tab2">'''

# Here we build the html of the tab with the plots
# We render the table and the links to the plots


jp = os.path.join( pp, "../data/vdM_Fills.json")
fd = open( jp, 'r')
fills = json.load( fd )
fd.close()

pagehtml += '<div style="padding:10px;"><div class="2col_left" style="text-align:justify; display:inline-block; width:50%">'
pagehtml += '<table class="smallsimple"><thead>'
pagehtml += "<tr><th>Fill</th><th>Type</th><th>Date</th><th>Description</th></tr></thead>\n<tbody>\n"

for fill in fills:
    pagehtml += "<tr>"
    pagehtml += '<td><span class="link" onclick=plotMagnetCurrents('+str(fill['fillno'])+')>' + str(fill['fillno']) + "</span></td>"
    pagehtml += "<td>" + fill['type'] + "</td>"
    pagehtml += "<td>" + fill['date'] + "</td>"
    pagehtml += "<td>" + fill['descr'] + "</td>"
    
    pagehtml += "</tr>"
pagehtml += "</tbody>\n</table>\n"
pagehtml += '</div><div class="2co_right" style="text-align:justify;vertical-align: top; margin-top:50px; display:inline-block; width: 49%;" >'
pagehtml += 'By clicking on the fill numbers in the table on the left the Raw data of the Corrector Magnet currents used for the vdM scans is plotted. You can zoom into the plots, pan around and download png files of the plots. Single curves can be switched on and off by clicking on the legend. A fast double click toggles the display of all traces.<br>The lower plot shows the same currents but they are plotted on top of each other using the first point of the curve as "anchoring point" If orbit drifts occur currents are not anymore on top of each other after optimisation scans.<br>The third plot shows "beautiful pictures" of the various scans: the magnet currents are anchored at 0 at the beginning of the fill and have the amplitudes of all curves normalised to the same value. There is one trace per beam and per plane.<br>Sometimes the "plateaux" have an apparent slope and seem to have a varying current. This is an artefact of the logging system. New values are only logged when they change wrt to the previous value. If for a given time nothing changes and then the current starts changing there are two points in time with different values which are connected by a line in the plot.<br>'


pagehtml += '</div>'
pagehtml += "</div><hr>"


pagehtml += '<div id="message"></div>'
pagehtml += '<H2>Plots of currents (raw data)</H2><div id="currentplot"></div>'
pagehtml += '<H2>Plots of currents (anchored at 0)</H2><div id="currentplot_norm"></div>'
pagehtml += '<H2>Plots of currents (anchored and normalised to equal amplitude)</H2><div id="currentplot_pict"></div>'


pagehtml += '</div>'
pagehtml += '''

<hr />
<div style="position: relative; text-align: justify;"><em>This web site is owned by the account <a href="http://consult.cern.ch/xwho?lumipog">lumipog</a> </em>.</div>
</body>
</html>
'''



print ("Content-type: text/html")
print ('')
print (pagehtml)

