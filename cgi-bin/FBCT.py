#!/usr/bin/python3
import cgi
import cgitb
import os
import json
import re
import sys
import json
from glob import glob

cgitb.enable()

pp = os.path.dirname(os.environ["SCRIPT_FILENAME"])

form = cgi.FieldStorage()

def getplot( fn ):
    fd = open(fn,'r')
    div = fd.read()
    fd.close()
    return div

def getInfo():
    filename = "../data/FBCT/plots/info.json"
    fd = open( filename, "r")
    info = json.load( fd )
    fd.close()
    return info

def ajaxplots(fill):
    filename = "../data/FBCT/plots/"
    plots = glob( os.path.join( filename, fill + "*.div" ))
    plots = sorted(plots)
    filename_early = plots[0]
    filename_late = plots[1]
    info = getInfo()
    message = ""
    if fill in info:
        message = "<strong>Note: </strong>" + info[fill]
    res = { 'early' : getplot( filename_early ),
            'late' : getplot( filename_late ),
            'message' : message
            }
    print ("Content-type: application/json")
    print ("")
    print (json.dumps( res ))
    
    return

# Check if this is an Ajax request to get the plot data
if "fillno" in form:
    fill = form['fillno'].value
    ajaxplots( fill )
    sys.exit()

pagehtml = '''<!DOCTYPE HTML >
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <title>CMS Lumi POG</title>
    <link rel="stylesheet" type="text/css" href="../css/site.css">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/plotly.min.js"></script>
    <script src="../js/dorosPlots.js"></script>
    <script src="../js/tab.js"></script>
  </head>
  
  <body><!-- page header -->
<table style="width:100%">
	<tbody>
		<tr>
			<td style="width: 72px; text-align: justify;"><img alt="CERN" height="72" src="../images/CMS-Color-Label.gif" width="72" /></td>
			<td>
			<p class="header-headline">FBCT plots for vdM Fills</p>

			<p class="center"><a href="../index.html">LumiPOG home</a></p>
			</td>
			<td style="width: 72px;"><img height="72" src="../images/LumiPOG.gif" width="72" /></td>
		</tr>
	</tbody>
</table>

<hr />


<div id="pageContent">

<div id="tab4">'''

# Here we build the html of the tab with the plots
# We render the table and the links to the plots


jp = os.path.join( pp, "../data/vdM_Fills.json")
fd = open( jp, 'r')
fills = json.load( fd )
fd.close()

pagehtml += '<div style="padding:10px;"><div class="2col_left" style="text-align:justify; display:inline-block; width:50%">'
pagehtml += '<table class="smallsimple"><thead>'
pagehtml += "<tr><th>Fill</th><th>Type</th><th>Date</th><th>Description</th></tr></thead>\n<tbody>\n"

for fill in fills:
    pagehtml += "<tr>"
    pagehtml += '<td><span class="link" onclick=plotRawFBCT('+str(fill['fillno'])+')>' + str(fill['fillno']) + "</span></td>"
    pagehtml += "<td>" + fill['type'] + "</td>"
    pagehtml += "<td>" + fill['date'] + "</td>"
    pagehtml += "<td>" + fill['descr'] + "</td>"
    
    pagehtml += "</tr>"
pagehtml += "</tbody>\n</table>\n"
pagehtml += '</div><div class="2co_right" style="text-align:justify;vertical-align: top; margin-top:50px; display:inline-block; width: 50%">'
pagehtml += 'By clicking on the fill numbers in the table on the left the Raw data of the FBCT are plotted. Two plots are shown: One at a very early time in the fill and one towards the end of the fill. This allows to estimate the evolution of the intensities during the fill (For PbPb the intensity is significant and burn off dominated and hence you have to imagine an exponential decrease of the intensities.) You can zoom into the plots, pan around and download png files of the plots. Single curves can be switched on and off by clicking on the legend. A fast double click toggles the display of all traces.<br><br><br> '

pagehtml += '</div>'
pagehtml += "</div><hr>"


pagehtml += '<div id="message" style="margin:20px" ></div><div id="plot_early" ></div><p></p><div id="plot_late"></div>'

pagehtml += '</div>'


pagehtml += '''

<hr />
<div style="position: relative; text-align: justify;"><em>This web site is owned by the account <a href="http://consult.cern.ch/xwho?lumipog">lumipog</a> </em>.</div>
</body>
</html>
'''



print ("Content-type: text/html")
print ('')
print (pagehtml)

