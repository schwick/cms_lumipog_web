#!/usr/bin/python3
import cgi
import cgitb
import os
import json
import re
import sys
import json
from glob import glob
from pileupLib import *

cgitb.enable()

pp = os.path.dirname(os.environ["SCRIPT_FILENAME"])

form = cgi.FieldStorage()

def err(kwargs):
    pass
    sys.stderr.write( kwargs )

# Check if this is an Ajax request to get the plot data

if os.environ['REQUEST_METHOD'] == 'POST':
    params = {}
    params['fill_length'] = int(form['fill_length'].value)
    params['levelling_time'] = int(form['levelling_time'].value)
    params['n_bunch'] = int(form['bunches'].value)
    params['hadxsec'] = float(form['hadronic_xsec'].value) * 1000
    params['initinstlumi'] = float(form['init_inst_lumi'].value) * 1e4
    params['btb'] = form['btb'].value
    params['btbrelval'] = form['btbrelval'].value
    params['btbconstval'] = form['btbconstval'].value
    params['scenario'] = form['scenario'].value
    params['atTime'] = int(form['at_specific_time'].value)
    params['timeInFill'] = float(form['time_in_fill'].value)

    #err("parsed " + str(params))

    if "poisson" not in form:
        params['poisson'] = False
    else:
        params['poisson'] = True
    if "lumiweight" not in form:
        params['lumiweight'] = False
    else:
        params['lumiweight'] = True

    (df, dfslice, title) = processInput(params)

    timeInFill = -1
    if params['atTime'] == 1 :
        timeInFill = 60*params['timeInFill']
        pileupplot,data,debug = makePileupPlotAtTime( params, dfslice, title, params['lumiweight'] )
        #pileupplot,data,debug = makePileupPlot( params, dfslice, title, params['lumiweight'] )
    else:
        pileupplot,data,debug = makePileupPlot( params, dfslice, title, params['lumiweight'] )

    beamplot = makeBeamPlot(dfslice, title, timeInFill)
    btbplot = makeBtBvar(timeInFill)

    res = { 'pileupplot' : pileupplot,
            'beamplot' : beamplot,
            'btbplot' : btbplot,
            'pileupdata' : data,
            'debug' : debug }
    print ("Content-type: application/json")
    print ("")
    print (json.dumps( res ))
    sys.exit()

pagehtml = '''<!DOCTYPE HTML >
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <title>CMS Lumi POG</title>
    <link rel="stylesheet" type="text/css" href="../css/site.css">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/plotly.min.js"></script>
    <script src="../js/utils.js"></script>
    <script src="../js/pileup.js"></script>
    <script src="../js/tab.js"></script>
  </head>

  <body onload="dotab()"><!-- page header -->
<table style="width:100%">
	<tbody>
		<tr>
			<td style="width: 72px; text-align: justify;"><img alt="CERN" height="72" src="../images/CMS-Color-Label.gif" width="72" /></td>
			<td>
			<p class="header-headline">GGuessing a Pileup Distribution</p>

			<p class="center"><a href="../index.html">LumiPOG home</a></p>
			</td>
			<td style="width: 72px;"><img height="72" src="../images/LumiPOG.gif" width="72" /></td>
		</tr>
	</tbody>
</table>

<hr />


<script>
  function dotab() {
  var tabber = new Tabber( "pageContent" );
  tabber.register("Pileup Distribution Estimates", "tab1");
  tabber.register("Documentation", "tab2");
  tabber.render();
}
</script>

<div id="pageContent">


<div id="tab1">

<div id="parameters">

<table id="param_tab">
<tbody>
</tbody>
<form id="pileupform">
<tr><th class="right">Fill length [min]</th><th>:</th><td><input type="number" name="fill_length" value="720"/></td></tr>
<tr><th class="right">Levelling time[min]</th><th>:</th><td><input type="number" name="levelling_time" value="60"/></td></tr>
<tr><th class="right">Number of colliding bunches</th><th>:</th><td><input type="number" name="bunches" value="2736"/></td></tr>
<tr><th class="right">Hadronic Cross Section [mb]</th><th>:</th><td><input type="number" name="hadronic_xsec" value="72.0"/></td></tr>
<tr><th class="right">Scenario</th><th>:</th><td><select  name="scenario"/>
<option value="2022" select="selected">2022</option>
<option value="2023">2023 / 2024</option>
</select</td></tr>
<tr><th class="right">Initial inst. luminosity [10<sup>38</sup>&thinsp;Hz&thinsp;/&thinsp;m<sup>2</sup>]</th><th>:</th><td><input type="number" name="init_inst_lumi" value="2.0"/></td></tr>
<tr><th class="right" style="vertical-align:top;">Bunch to Bunch luminosity variations</th><th style="vertical-align:top;"> : </th><td>
  <table><tbody>
    <tr><td><input name="btb" type="radio" value="variable" checked="checked"/></td> <td><label>Increasing Bunch to Bunch variations throughout the fill (recommended)</label></td></tr>
    <tr><td><input name='btb' type="radio" value="relative"/</td><td><label>Fixed relative bunch to bunch variation in [%] (Sigma / Mean) : </label> <input type=number value="18" name="btbrelval"/></td></tr>
    <tr><td><input name='btb' type="radio" value="constant"/</td><td><label>Constant bunch to bunch variation in (Sigma) : </label> <input type=number value="0.5" name="btbconstval"/></td></tr>
  </tbody></table>
<tr><th class="right">Weight distribution with inst. luminosity</th><th>:</th><td><input type="checkbox" name="lumiweight" checked="checked" value="1"/></td></tr>
<tr><th class="right">Include Poisson Statistic for Bunch pileup</th><th>:</th><td><input type="checkbox" name="poisson" value="1"/> (Processing will become slow!)</td></tr>
<tr><td></td><td></td><td><button type="button" onclick="pileupSubmit( false )" />Make plots</td></tr>
<tr><th class="right">Time into the fill [min]</th><th>:</th><td><input type="number" name="time_in_fill" value="60.0"/></td></tr>
<tr><td></td><td></td><td><button type="button" onclick="pileupSubmit( true )" />Plots at specific time in fill</td></tr>
<input type="hidden" id="atTime" name="at_specific_time" value="0"/>
</form>
  </tbody></table>
</td></tr>


</td> </tr>
</table>

<p id="Waiter">Waiting for data ...</p>
<div id="pileup_download"><a id="downlink" download="pileup_distribution.json" type='text/json'>Download pileup distribution</a></div>

<div id="pileup_download_cmssw"><a id="downlink_cmssw" download="pileup_distribution_cfi.py" type='text/json'>Download pileup distribution for CMSSW simulation</a></div>
</div>


<div id="message" style="margin:20px" ></div><div id="plot_pileup" ></div><p></p><div id="plot_beam"></div><p></p><div id="plot_btb"></div>

</div>

<div id="tab2">
<h2>Introduction</h2>
<p>
The estimations for the pileup distributions are based on Fills simulated by the LHC colleagues with their Luminosity model. This model does not include all known or observed effects. The model only yields an average bunch luminosity for the fill. It does not reflect the bunch to bunch variations of the bunch luminosity which occurs due effects like long range interactions at the experiments or eclectron cloud effects which vary heavily according to the postiion of the bunch in the filling scheme.
</p><p>
Bunch to bunch variations of the luminosity have been extensively studied by the LHC colleagues based on 2018 data. A summary can be found in the writeup <a target="_blank" href="../documents/THPAB172.pdf">THPAB172</a>. Since bunch to bunch variations are important for the realistic estimation of the pileup in a fill, these variations are inserted a posteriori into this small simulation program. The variations are based on a second order polynomial approximation of the curve in Figure 7 of the mentioned document, which represents the average evolution of the bunch-to-bunch luminosity variations for all bunches in the fill.
</p>
<p>
Two best case scenarios have been simulated:
<ul><li>For the year 2022.</li>
<li>For the period 2023-2024.</li>
</ul>
The fills should be considered as ideal "best case" fills. It is hoped that these fills can be produced towards the end of the respective running periods, however they should not be taken as an average fill for the running period.
</p>
<h2>Usage of the Simulation</h2>
<p>
The Pileup Estimation tool is based on simulated fills of a length of 20 hours. The maximal levelling time in 2022 is 6h whereas in the 2023/2024 period the maximal levelling time is 13h. Input values not compatible with the range of the simulated data will be truncated to the values of the simulation. It is possible to enter negative levelling times. This means the simulation will then start the corresponding amount of time AFTER the levelling has stopped and hence the initial luminosity will be below 2e34 Hz/cm<sup>2</sup>. For example entering -60 minutes results in a peak luminosity of 1.81e34 Hz/cm<sup>2</sup>)
</p>

<p>The default for the simulation is to NOT fold in the Poisson distribution which describes the variation of the pileup for bunches at fixed luminosity. The reason is that the CMS MonteCarlo generators are generating these Poisson distributions when simulating events and this tool is thought to generate the input for Monte Carlo productions. Howver by checking the correspondent option one can enable the generation of Poisson statistics and hence the tool will calculate the pileup distribution as it is expected to be seen in the experiment during data taking.
</p>
<p>
The pileup distribution can be normalised in two different ways. The default shows the fraction of
event at a given pileup weighted with the instantaneous luminosity on the y-axis. The option "Weight distribution with luminosity" can also be unchecked. The luminosity weighting is switched on by default since most of the physics triggers are scaling with the instantaneous luminosity and hence a weighted distribution is desired for the Monte Carlo generation. This option also allows to compare the distribution with the output of "pileupCalc.py" which also gives luminosity weighted distributions.
</p>
<p>
The middle plot shows the evolution of essential beam parameters according to the simulation of the LHC experts. Due to the various y scales for the parameters it is useful to insepct these parameters one by one. You can disable or enable the parameters on the plot by clicking on the legend item. (You can also drag on the x and y axis towards their end-points or in the middle or you can zoom into a rectangle on the plot.) On the top right some too buttons appear if you approach the region with the mouse.
</p>

<p>The field "Time into the fill [min]" with the button "Plots at specific time in fill" can be used to generate a pileup distribution for a specific moment in the fill (i.e. NOT integrate over the entire fill). This is useful to investigate the pileup at a specific during the fill. For example the beginning and the end of the levelling time can be compared (only small differences are expected due to the emittance growth of the beams).
</p>

</div>

</div>

'''


pagehtml +=  '''
<hr />
<div style="position: relative; text-align: justify;"><em>This web site is owned by the account <a href="http://consult.cern.ch/xwho?lumipog">lumipog</a> </em>.</div>
</body>
</html>
'''



print ("Content-type: text/html")
print ('')
print (pagehtml)
