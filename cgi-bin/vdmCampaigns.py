#!/usr/bin/python3
import cgi
import cgitb
import os
import json
import re

cgitb.enable()

pp = os.path.dirname(os.environ["SCRIPT_FILENAME"])

#form = cgi.FieldStorage()
tp = os.path.join( pp, "../templates/template.html")
fd = open( tp, 'r')
starthtml = ""
endhtml = "    <!-- page content end -->"
state = "start"
for line in fd:
    if state == "start":
        starthtml += line
        if re.search( "<!-- page content -->", line ):
            state = "content"
    elif state == "content":
        if re.search( "<!-- page content end -->", line ):
            state = "end"
    elif state == "end":
        endhtml += line
fd.close()
        
html = "<h2>Table of vdM Fills in CMS<h2>\n"

jp = os.path.join( pp, "../data/vdM_Fills.json")
fd = open( jp, 'r')
fills = json.load( fd )
fd.close()

html  = '<p>Download the following table as <a href="../data/vdM_Fills.json" target="_blank">json file</a></p>'
html += '<table class="clearAndSimple"><thead>'
html += "<tr><th>Fill</th><th>Type</th><th>Date</th><th>Description</th></tr></thead>\n<tbody>\n"

for fill in fills:
    html += "<tr>"
    html += "<td>" + str(fill['fillno']) + "</td>"
    html += "<td>" + fill['type'] + "</td>"
    html += "<td>" + fill['date'] + "</td>"
    html += "<td>" + fill['descr'] + "</td>"
    
    html += "</tr>"
html += "</tbody>\n</table>\n"

html = starthtml + html + endhtml
html = html.replace("__to_base__", "../")
html = html.replace("__page_title__", "Table of CMS vdM Fills")
    
print ("Content-type: text/html")
print ('')
print (html)



