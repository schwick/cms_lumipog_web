import sys
import os
import pandas as pd
import plotly.graph_objects as go
import plotly.express as px
import plotly.io as pio
import numpy as np
import math
from scipy.stats import poisson
pio.templates.default = "plotly_dark"

sqrt2pi = math.sqrt(math.pi*2)

def err(kwargs):
    pass
    sys.stderr.write( kwargs )

def rmsEvolution(time):
    '''Let's become fancy: evolution of the rms of the bunch lumis during
    the fill according to Ilias & co paper THPAB172. Since we do not have
    the data of Figure 7 this approximation was done "by eye". A quadratic
    function is not so great (especially if we go beyond 800min it will
    overestimate the rms which clearly slows growing in the paper...).
    However, good enough for the purpose here I would say.'''
    rms = 0.12 + time * time * 2.45e-7
    return rms

def poissonLumiProb( avgpu ):
    '''avgpu contains probability vector for all pileup values considered (e.g. 0...100)
    Here we need to loop over all these pileup values, create a poisson distribution
    weight it with the probability of the avg pilup and then add all of these up.
    '''

    res = np.zeros(len(avgpu))
    pus = np.arange(0,len(avgpu),1)
    pu = 0
    for apu in avgpu:
        pupoisson = poisson.pmf( pus, pu )
        res += apu * pupoisson
        pu += 1

    return res,pus

def testPoisson( tpu ):
    pu = np.zeros(101)
    pu[tpu] = 1
    (res,pus) = poissonLumiProb( pu )
    fig = go.Figure()
    fig = px.line( x=pus,y=res )
    fig.write_html("plots/testPoisson.html",include_plotlyjs="directory")

def puProb( pus, avgPu, sig = None, rel_sig=None, poisson = False ):
    '''
    Gaussian probabilities for discrete pileup values.

    sig is rms of pileup
    relsig is sig/avgPU if sig = None

    prob is normalised to one is a vector of probabilities which gives a
    probability for each integer pileup value. All probabilities sum up
    to '1'
    '''

    if sig == None:
        sig = rel_sig*avgPu
    prob = 1/(sig*sqrt2pi) * np.exp(-0.5*(pus-avgPu)*(pus-avgPu)/(sig*sig))

    # We should fold with a Poisson if desired
    if poisson:
        prob,tmp = poissonLumiProb(prob)

    return prob


# Unit conversion (lumi to pileup using 80mb)
def lumiToPu( lumi, hadxsec ):
    return lumi * hadxsec / 11245


def makeBtBvar(timeInFill):
    timev = np.arange(0, 850, 10 )
    rmsevo = rmsEvolution(timev)
    fig = px.line(x=timev,y=100*rmsevo, labels= {'x':'TIme [min]','y':"rms [% of σ/μ]"},
                  title= "Evolution of RMS bunch by bunch lumi")
    if timeInFill >= 0:
        fig.add_vline( x=timeInFill/60, line_width = 3, line_color = "green" )
        pass
    fig.update_yaxes( range=[0,30])
    res = fig.to_html( include_plotlyjs=False )
    return res


def processInput( params ):
    msg = ""
    if params['scenario'] == "2022":
        infile = "../data/LHCLumiPrediction/performance_2022.csv"
    elif params['scenario'] == "2023":
        infile = "../data/LHCLumiPrediction/performance_2023-2024.csv"

    df = pd.read_csv( infile )
    df['int lumi per step'] = df['integrated luminosity per fill (pb-1)'].diff()
    df['avg bunchlumi'] = df['inst. luminosity (Hz/m2)']/params['n_bunch'] / 1e34 * params['initinstlumi']/2.0
    df = df.set_index(df['Time (s)']/60)

    maxlevtime = df['leveling time (h)'][0] * 60
    if maxlevtime < params['levelling_time'] :
        msg +=  "Maximal levelling time is "+ str(maxlevtime) + "min. Using that one."
        params['levelling_time'] = maxlevtime

    start_min = round(maxlevtime - params['levelling_time'])
    stop_min  = round(params['fill_length'] + start_min)
    if stop_min > 1200:
        stop_min = 1200

    if params['btb'] == "relative":
        sigstr = "      relative σ of bunch-lumi rms : {:4.1f}%".format( float(params['btbrelval']) )
    elif params['btb'] == "variable":
        sigstr = "      relative σ of bunch-lumi rms varying from: {:4.1f}% to {:4.1f}% in 8h".format( 12.5, 25 )
    else:
        sigstr = "      constant σ of bunch lumi rms : {:4.2f}".format( float(params['btbconstval']) )

    title = "Fill Length : {:5.2f}h      Levelling time : {:5.2f}h".format( float(params['fill_length'])/60, float(params['levelling_time'])/60) + sigstr
    if params['atTime'] == 1 :
        title = title + "<br><sup>Pileup plot for " + str(params['timeInFill']) + " min into the fill.</sup>"

    df['integrated luminosity per fill (pb-1)'] = df['integrated luminosity per fill (pb-1)'] - df.loc[start_min,'integrated luminosity per fill (pb-1)']
    dfslice = df.loc[start_min:stop_min]

    return df, dfslice, title



def makeBeamPlot(dfslice, title, timeInFill):
    fig = px.line( dfslice, x=dfslice.index-dfslice.index[0], y=["pileup (events)", 'inst. luminosity (Hz/m2)', 'integrated luminosity per fill (pb-1)', 'avg bunchlumi', 'phi/2 IP1/IP5 (urad)', 'beta_star IP1/IP5 (m)', 'emit_x (um rad)', 'emit_y (um rad)'], title = title, labels={ "index" : "Time [min]" }  )
    if timeInFill >= 0 :
        fig.add_vline( x=timeInFill/60, line_width = 3, line_color = "green" )
        pass
    beamPlot = fig.to_html(include_plotlyjs=False)
    return beamPlot


def makePileupPlotAtTime( params, dfslice, title, lumiweight = False ):
    '''fillTime is the time into the fill at which the pileup should be calculated (in minutes)'''
    timeInFill = params['timeInFill'] # handle time minutes in this routine
    pileups = np.arange( 0,101,1 )
    pudist = np.zeros( len(pileups) )
    t0 = dfslice.index[0]
    debug = ""
    tcmp = 9999999999999999
    for ix,row in dfslice.iterrows():
        t = ix - t0
        if abs(t-timeInFill) < tcmp:
            tcmp = abs(t-timeInFill)
            continue
        avglum = row['avg bunchlumi']
        if params['btb'] == "variable":
            relsig = rmsEvolution( t )
            csig = None
        elif params['btb'] == 'relative':
            relsig = float(params['btbrelval']) / 100
            csig = None
        elif params['btb'] == 'constant' :
            csig = lumiToPu( float(params['btbconstval']),float(params['hadxsec']) )
            relsig = 0

        avgpu   = lumiToPu( avglum, float(params['hadxsec']) )
        debug += " " + str(avgpu)
        puprob  = puProb( pileups, avgpu, sig = csig, rel_sig = relsig, poisson = params['poisson'] )

        if lumiweight :
            lumstep = 60*row['inst. luminosity (Hz/m2)']
            pudist  = pudist + puprob * lumstep
        else:
            pudist = pudist + puprob

        break

    # Normalise the pudist to 1 if no lumiweight
    if lumiweight:
        #pudist = pudist * 1e-40
        #ytit = "dL/dμ [pb-1]"
        norm = pudist.sum()
        pudist = pudist/norm
        ytit = "inst. lumi weighted fraction of events [%]"
    else:
        norm = pudist.sum()
        pudist = pudist/norm
        ytit = "fraction of events [%]"

    fig = px.line( x=pileups, y=pudist * 100, title=title, labels = { 'x' : 'Pileup', 'y' : ytit}, line_shape="hvh")
    plot = fig.to_html( include_plotlyjs='False')

    data = { 'pileup_values' : pileups.tolist(),
             'fraction' : pudist.tolist() }
    return plot, data, debug


def makePileupPlot( params, dfslice, title, lumiweight = False ):
    pileups = np.arange( 0,101,1 )
    pudist = np.zeros( len(pileups) )
    t0 = dfslice.index[0]
    debug = ""
    for ix,row in dfslice.iterrows():
        avglum = row['avg bunchlumi']
        t = ix - t0
        if params['btb'] == "variable":
            relsig = rmsEvolution( t )
            csig = None
        elif params['btb'] == 'relative':
            relsig = float(params['btbrelval']) / 100
            csig = None
        elif params['btb'] == 'constant' :
            csig = lumiToPu( float(params['btbconstval']),float(params['hadxsec']) )
            relsig = 0

        avgpu   = lumiToPu( avglum, float(params['hadxsec']) )
        debug += " " + str(avgpu)
        puprob  = puProb( pileups, avgpu, sig = csig, rel_sig = relsig, poisson = params['poisson'] )

        if lumiweight :
            lumstep = 60*row['inst. luminosity (Hz/m2)']
            pudist  = pudist + puprob * lumstep
        else:
            pudist = pudist + puprob

    # Normalise the pudist to 1 if no lumiweight
    if lumiweight:
        #pudist = pudist * 1e-40
        #ytit = "dL/dμ [pb-1]"
        norm = pudist.sum()
        pudist = pudist/norm
        ytit = "inst. lumi weighted fraction of events [%]"
    else:
        norm = pudist.sum()
        pudist = pudist/norm
        ytit = "fraction of events [%]"

    #fig = go.scatter()
    #fig.add_trace( go.Scatter( x-pileups, y=pudist*100, labels = { 'x' : 'Pileup', 'y' : ytit}, line_shape="hvh" )
    fig = px.line( x=pileups, y=pudist * 100, title=title, labels = { 'x' : 'Pileup', 'y' : ytit}, line_shape="hvh")
    plot = fig.to_html( include_plotlyjs='False')

    data = { 'pileup_values' : pileups.tolist(),
             'fraction' : pudist.tolist() }
    return plot, data, debug

######################## MAIN #########################
