#!/usr/bin/python3
import cgi
import cgitb
import os
import json
import re
import sys
import json

cgitb.enable()

pp = os.path.dirname(os.environ["SCRIPT_FILENAME"])

form = cgi.FieldStorage()

def getplot( fn ):
    div = ""
    try :
        fd = open(fn,'r')
        div = fd.read()
        fd.close()
    except:
        # Probably plot not available
        pass
    return div

def getInfo():
    filename = "../data/DOROS/plots/info.json"
    fd = open( filename, "r")
    info = json.load( fd )
    fd.close()
    return info

def ajaxplots(fill):
    filename = "../data/DOROS/plots/"
    filename_lr = os.path.join( filename, fill + "_DOROS_lr.div" )
    filename_avg = os.path.join( filename, fill + "_DOROS_avg.div" )
    info = getInfo()    
    message = "no remarks for this fill"
    if fill in info:
        message = info[fill]
    res = { 'lr' : getplot( filename_lr ),
            'avg' : getplot( filename_avg ),
            'message' : "<strong>Info for fill " + fill + " : </strong>" + message
            }
    print ("Content-type: application/json")
    print ("")
    print (json.dumps( res ))
    
    return

# Check if this is an Ajax request to get the plot data
if "fillno" in form:
    fill = form['fillno'].value
    ajaxplots( fill )
    sys.exit()

pagehtml = '''<!DOCTYPE HTML >
<html lang="en">

  <head>
    <meta charset="UTF-8">
    <title>CMS Lumi POG</title>
    <link rel="stylesheet" type="text/css" href="../css/site.css">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/plotly.min.js"></script>
    <script src="../js/dorosPlots.js"></script>
    <script src="../js/tab.js"></script>
  </head>
  
  <body onload="dotab()"><!-- page header -->
<table style="width:100%">
	<tbody>
		<tr>
			<td style="width: 72px; text-align: justify;"><img alt="CERN" height="72" src="../images/CMS-Color-Label.gif" width="72" /></td>
			<td>
			<p class="header-headline">BPM Know How</p>

			<p class="center"><a href="../index.html">LumiPOG home</a></p>
			</td>
			<td style="width: 72px;"><img height="72" src="../images/LumiPOG.gif" width="72" /></td>
		</tr>
	</tbody>
</table>

<hr />

<script>
  function dotab() {
  console.log( "dotab" );
  var tabber = new Tabber( "pageContent" );
  tabber.register("Doros Doc", "tab1");
  tabber.register("arcBPM Doc", "tab2");
  tabber.register("Ext. Docs", "tab3");
  tabber.register("Doros Plots", "tab4");
  tabber.render();
}
</script>

<div id="pageContent">
<div id="tab1">

<h3 style="text-align: justify;">General remarks</h3>

<p style="text-align: justify;">The information on this page are based on an exchange with W. Kozanecki from ATLAS and Marek Gasior, the LHC expert of the DOROS readout electronics. The original document with questions to the experts and their answers can be found here:</p>

<p style="text-align: justify;"><a href="https://docs.google.com/document/d/1RulqedsNojD3KVVw-p6W5MRZrh6lkJhh4VnG4v-kqw0/edit#" target="_blank">https://docs.google.com/document/d/1RulqedsNojD3KVVw-p6W5MRZrh6lkJhh4VnG4v-kqw0/edit#</a></p>

<p style="text-align: justify;">The DOROS electronics has been updated after 2016. Everything below refers to the updated system. Corrections for the old hardware used in 2015 and 2016 are calculated assuming the electronics in 2015 and 2016 was the same as after 2016 and then the corrections are &quot;extrapolated&quot;. Marek assumes that these extrapolated corrections do not necessarily need to be worse than the corrected data in 2017 and afterwards. One should carefully look at the data and watch out for inconsistencies. (??? What does &quot;extrapolation&quot; mean here???)</p>

<p style="text-align: justify;">Correction are done online from fill 6371 onwards (9 Nov 2017), i.e. from this fill on Timber/NXCALS contains the corrected data.</p>

<h3 style="text-align: justify;">Data coming from the DOROS</h3>

<p style="text-align: justify;">In Timber and in DIP data of the DOROS BPMs is logged. In principle the two data sets should be the same. In TImber you find a lot of&nbsp;DOROS related data. The Timber variables interesting for the vdM analysis of the experiments are:</p>

<p style="text-align: justify;">LHC.BPM.{loc}.B[1|2]_DOROS_POS_[H|V] where loc is the location. For CMS this is 1L5 or 1R5.</p>

<p style="text-align: justify;">The data is presented in units of [mm]. The data for each DOROS data contains an arbitrary meaningless offset. Therefore only differences of positions are interesting quantities. The calibration of the lenght scale of the DOROS data is not sufficiently precise to be used in the vdM scans. Therefore the length scale needs to be re-calibrated using CMS inner tracker data.&nbsp;</p>

<p style="text-align: justify;">The DOROS calibration (i.e. the conversion of the signal amplitude to a position in mm) depends on the intensity of the beam. As long as the intensity during a fill does nor change the calibration stays constant. This is to a good approximation the case in pp vdM fills. However the intensity changes a lot during a PbPb vdM fill. Here corrected data is provided by the LHC experts.&nbsp;</p>

<p style="text-align: justify;">The DOROS data from Heavy Ion run have a lower S/N since the charge of the Ion beam is significantly lower. The absolute amplitudes of the signals are of the same range as the pp data since the gain of the amplifiers are automatically increased (see below).</p>

<p style="text-align: justify;">&nbsp;</p>

<h3 style="text-align: justify;">DOROS Data processing on the LHC side</h3>

<p style="text-align: justify;">The input to the DOROS data processing are the raw electrode signals from the BPM detectors. Two pick up buttons for each plane (Horizontal and Vertical H/V) are placed around the beam pipe. Data is then processed in electronics and software.</p>

<h4 style="text-align: justify;">Amplitide corrections</h4>

<p style="text-align: justify;">In the laboratory a representative DOROS reference electronics has been set up. The electronics is fed with RF signals emulating the beam pickup signals. The amplitudes of these signals are varied linearly and the response of the electronics is measured. (The DC envelop of the RF signals is measured). Then a correction is calculated such that if applied to the raw electrode signals, the response is linear too. This correction is a 3rd order polynomial. The resulting corrections are within 1% of the original signals. These corrections are applied to the pure raw electrode signals (independently for each electrode) and hence the correction is independent of the beam position. In order to make the system to large extend independent of the beam intensity, the electronics changes gain if the input signal changes amplitude, so that the signal amplitude after the first stage amplifier remains within 10%. This switching is performed by a RF envelop detector which measures the envelope of the incoming RF signals. However, dthe residual non-linearitly of the system is the dominating the systems non-linearity. A further technical detail is that the system can run with three different time constants. For all of these different conifgurations the response of the RF envelope detector is slightly different, and hence different correction polynomials are calculated for all of these three configurations. It is assumed that all DOROS units have the same nonlenearity erros and the same corrections are applied for all units. This is justified by the assumption that the spread of the erros between the units is one order of magnitude smaller tha the errors themselves.</p>

<h4 style="text-align: justify;">Position calculation</h4>

<p style="text-align: justify;">The DOROS positions are calculated from the 4 corrected amplitude signals. If Ac (Cc) and Bc (Dc) are the corrected amplitude signals of the horizontal (vertical) pickups the horizontal (vertical) position is calculated</p>

<p style="text-align: justify;">H = (Ac-Bc) / (Ac+Bc)&nbsp;&nbsp;&nbsp; and correspondingly V = (Cc -Dc) / (Cc+Dc)</p>

<p style="text-align: justify;">The resulting values are called normalised H and V positions.</p>

<p style="text-align: justify;">The absolute positions are calculcated from these normalised positions with a 5th orde 2D polynom, which describes the &quot;geometrical response&quot; of the BPM. Since the polynomial is 2 dimensional there is a coupling between the 2 planes. The further from the BPM centre and its axis the larger the mutual influence of the planes is.</p>

<p style="text-align: justify;">Different types of BPMs have different geometrical response polynomials.</p>

<p style="text-align: justify;">When the beams are far of the centre of the BPM the electrode signal amplitudes are very different (for the Q1 BPMs the difference is often a factor of 2). For these cases the error of the linearisation correction becomes more critical than for cases where the beams are close to the centre of the BPM. When the beams are centred and all amplitudes are the same the error on the position caused by an error on the amplitude of the corrected signal becomes small as can be seen in the formul for H an V above.</p>

<p style="text-align: justify;">&nbsp;</p>

<h3 style="text-align: justify;">Features of the DOROS data</h3>

<p style="text-align: justify;">By design the doros length scale changes non-linearly for large position offsets in the DOROS detector (large offsets from the nominal beam orbit). In addition there is a small &quot;cross talk&quot; between horizontal and vertical DOROS data in the same detector. This can be seen in the vdM scan.&nbsp;</p>

<h3 style="text-align: justify;">Corrected data</h3>

<p style="text-align: justify;"><span style="background-color:#ccffcc;">From Fill 6371 (9 Nov 2017</span>) onwards, the data in Timber is the intensity corrected data. Initially it was thought that for 2015 and 2016 the quality of the raw data used to derive the corrections was not good enough (a lot of missing data points) to extract the corrections. However, this was traced to a problem with the sampling of the data in Timber and it is believed now, that corrections for all fills in 2015 and 2016 can be derived. The corrected data before fill 6371 is provided in form of CVS files which contain the corrected <i>and</i> the uncorrected data. These csv files can be downloaded from the following table. Columns with the label pc containt corrected positions.</p>

<table border="1" cellpadding="1" cellspacing="1" class="clearAndSimple" style="width:100%;">
	<thead>
		<tr>
			<th scope="col" style="text-align: justify;">Fill</th>
			<th scope="col" style="text-align: justify;">Date</th>
			<th scope="col" style="text-align: justify;">Link</th>
			<th scope="col" style="text-align: justify;">Remark</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td style="text-align: justify;">4266</td>
			<td style="text-align: justify;">24/8/2015 pp</td>
			<td style="text-align: justify;"><a href="../data/DOROS/4266_P5_data_exper.csv" target="_blank">4266_P5_data_exper.csv</a></td>
			<td style="text-align: justify;">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align: justify;">4689</td>
			<td style="text-align: justify;">3/12/2015 PbPb</td>
			<td style="text-align: justify;"><a href="../data/DOROS/4689_P5_data_exper.csv" target="_blank">4689_P5_data_exper.csv</a></td>
			<td style="text-align: justify;">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align: justify;">7442</td>
			<td style="text-align: justify;">14/11/2018 Pb Pb</td>
			<td style="text-align: justify;"><a href="../data/DOROS/7442_P5_data_exper.csv" target="_blank">7442_P5_data_exper.csv</a></td>
			<td style="text-align: justify;">dumped due to 10Hz oscillations; corrected data in Timber</td>
		</tr>
		<tr>
			<td style="text-align: justify;">7443</td>
			<td style="text-align: justify;">14/11/2018 Pb Pb</td>
			<td style="text-align: justify;"><a href="../data/DOROS/7443_P5_data_exper.csv" target="_blank">7443_P5_data_exper.csv</a></td>
			<td style="text-align: justify;">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align: justify;">&nbsp;</td>
			<td style="text-align: justify;">&nbsp;</td>
			<td style="text-align: justify;">&nbsp;</td>
			<td style="text-align: justify;">&nbsp;</td>
		</tr>
		<tr>
			<td style="text-align: justify;">&nbsp;</td>
			<td style="text-align: justify;">&nbsp;</td>
			<td style="text-align: justify;">&nbsp;</td>
			<td style="text-align: justify;">&nbsp;</td>
		</tr>
	</tbody>
</table>

<h3 style="text-align: justify;">Raw electrode data correlated to Beam intensity</h3>

<p style="text-align: justify;">To investigate the dependence of the DOROS data as function of the beam intensity, the DOROS expert Marek Gasior gave us a csv file for the PbPb fill 7442 in 2018 which contains DCCT, integrated FBCT and raw DOROS electrode data.&nbsp;</p>

<p style="text-align: justify;">This file can be downloaded here:&nbsp; <a href="data/DOROS/DOROS_electrode_signals_in_PbPb18_vdM_scans.zip">DOROS_electrode_signals_in_PbPb18_vdM_scan.zip</a></p>
</div>
<div id="tab2">
<h2>Betatron fits to extrapolate the beam position at the IP</h2>
<p>The arcBPMs close to an IP can be used to extrapolate the beam position to the IP. This data can the be compare e.g. to the DOROS data. The algorithm used by the LHC software to extract this offset at the IP is documented in Section 4 of the following document (by Joerg Wenninger): <a href = "../documents/doc_algorithms_Steering-algos.pdf" target = "_blank">doc_algorithms_Steering-algos.pdf</a>
</p>
<p>
The procedure to perform the Betatron fits and to extract the results is descibed on the <a href="https://twiki.cern.ch/twiki/bin/view/CMS/LumiOrbitDrifts" target="_blank">LumiPOG Twiki for orit drifts</a> (Section "Get arc BPM beam positions". The original recipe to operate the relevant application in the technical network has been written by Witold Kozanecki and can be <a href="../documents/IP_position_hstry_howTo.txt" target="_blank">downloaded here</a>.
</p>
<h3>Optics files to be used for Betatron fits</h3>
<p>
The procedure to perform betatron fits require optics files to be chosen. A list of optics files up to the year 2016 can be found in the Instructions of Witold mentioned above. The following table gives information for later fills:
</p>

<table class="clearAndSimple">
  <thead>
    <tr><th> Fill No </th><th>Optics file</th> <th> Fill type</th><th>Remark</th></tr>
  </thead>
  <tbody>
    <tr><td>6380</td><td>R2017a_A310C310A10mL310</td><td>pp 5TeV vdM</td><td>Optics file name extracted from new ARC BPM data extraction tool.</td></tr>
  </tbody>
</table>

<p></p>

<h2>Temperature data for the arcBPMs</h2>
The temperature related data for the arc BPMs is stored in 2 vectors:<br>
LHC.BOFSU:DAB_TEMP_H<br>
LHC.BOFSU:DAB_TEMP_V<br>
They contain one temperature per BPM. The BPM positions in um are stored in the vectors:<br>
LHC.BOFSU:POSITIONS_H<br>
LHC.BOFSU:POSIIONS_V<br>
There is also metadata to find the name of the BPMs. 
</div>
<div id="tab3">
<p>
The following links point to external documentation which are useful for the topic.
</p>
<table class="clearAndSimple">
<thead>
<tr><th>link</th><th>description</th></tr>
</thead>
<tbody>
<tr><td><a href="https://docs.google.com/document/d/1RulqedsNojD3KVVw-p6W5MRZrh6lkJhh4VnG4v-kqw0/edit#heading=h.8lctb816u8a0" target="_blank">Google Doc</a></td>
<td>Google doc with Questions and answers to and from LHC experts</td></tr>
<tr><td><a href="../documents/presentations/LHC_BI_-_Lecture_1_-_Intro_and_BPM-2.pdf" target="_blank">talk</a></td>
<td>Lecture by Rhodri Jones with a lot of information on the BPMs of the LHC.</td></tr>
<tr><td><a href="../documents/presentations/201012_DOROS_general_info_for_experiments.pdf" target="_blank">talk</a></td>
<td>Presentation of Marek Gasior et al. on the DOROS system.</td></tr>
</tbody>
</table>
</div>

<div id="tab4">'''

# Here we build the html of the tab with the plots
# We render the table and the links to the plots


jp = os.path.join( pp, "../data/vdM_Fills.json")
fd = open( jp, 'r')
fills = json.load( fd )
fd.close()

pagehtml += '<div style="padding:10px;"><div class="2col_left" style="text-align:justify; display:inline-block; width:50%">'
pagehtml += '<table class="smallsimple"><thead>'
pagehtml += "<tr><th>Fill</th><th>Type</th><th>Date</th><th>Description</th></tr></thead>\n<tbody>\n"

for fill in fills:
    pagehtml += "<tr>"
    pagehtml += '<td><span class="link" onclick=plotRawDoros('+str(fill['fillno'])+')>' + str(fill['fillno']) + "</span></td>"
    pagehtml += "<td>" + fill['type'] + "</td>"
    pagehtml += "<td>" + fill['date'] + "</td>"
    pagehtml += "<td>" + fill['descr'] + "</td>"
    
    pagehtml += "</tr>"
pagehtml += "</tbody>\n</table>\n"
pagehtml += '</div><div class="2co_right" style="text-align:justify;vertical-align: top; margin-top:50px; display:inline-block; width: 50%">'
pagehtml += 'By clicking on the fill numbers in the table on the left the Raw data of the DOROS detectors in the Q1 on the left and right side of CMS are plotted. You can zoom into the plots, pan around and download png files of the plots. Single curves can be switched on and off by clicking on the legend. A fast double click toggles the display of all traces.<br><br><br> '
pagehtml += 'Two plots are displayed. The upper plot shows the raw DOROS position data separate for the DOROS detectors on the right and on the left side of the experiment. To get rid of large (and meaningless) offsets for each detector the mean over the fill has been subtracted. The second plot shows the average of the data from the left and the right side. Again a meaningless overall offset has been subtracted to get all traces more or less on the same baseline.'

pagehtml += '</div>'
pagehtml += "</div><hr>"


pagehtml += '<div id="message"></div><div id="plot_lr"></div><div id="plot_avg"></div>'


pagehtml += '</div>'
pagehtml += '''

<hr />
<div style="position: relative; text-align: justify;"><em>This web site is owned by the account <a href="http://consult.cern.ch/xwho?lumipog">lumipog</a> </em>.</div>
</body>
</html>
'''



print ("Content-type: text/html")
print ('')
print (pagehtml)

