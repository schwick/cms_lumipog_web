# cms_lumipog_web

Web pages for the CMS lumipog.

Since cern does not provide anymore standard apache websites on adequate filesystems 
(eos is not usable for websites due the performance problems when acccessing a bunch 
of files) this site had to created on another technical platform. CERN offers
containers in openshift which can house web applications. The docker container
which we use for this project is based on the cern CentOS 8 base container.
We add nginx to it and the fcgiwrapper to have simple cgi scripting. This will
hopefully do the job a simple reasonably performing website.

The website is based on the system develped for the LPC pages.

## Instructions
If you need to move this web site to a new web server remember to edit
the file

internal/data/config.json

In addition be aware that cgi scripts are contained in the directories cgi-bin and internal/cgi-bin.

The site requires python 3 as script handler.