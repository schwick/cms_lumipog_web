class Tabber {
    constructor( divid ) {
	this.divid = "div#" + divid;
	this.tabs = [];
    };
    
    register( name, tabid ) {
	this.tabs.push( { 'name' : name,
			  'tabid' : tabid } );
    };

    activate( tabid ) {
	$('div.tab').hide();
	$('div#'+ tabid).show();
	$('div.tabbutton').removeClass( "activetabbutton" );
	$('div#tabbut_' + tabid).addClass( "activetabbutton" );
    };

    render( name = undefined ) {
	$(this.divid).prepend( '<div id="tabbuttons"> </div>' )
	var tabstr = "";
	var mytabber = this;
	for ( var ix = 0; ix < this.tabs.length; ix++ ) {
	    var tab = this.tabs[ix];
	    console.log( tab.tabid, tab.name );
	    var html = '<div class="tabbutton" id="tabbut_' + tab.tabid + '" data-id="'+  tab.tabid + '">' + tab.name + '</div>'
	    $('div#tabbuttons').append( html );
	    $('div#tabbut_' + tab.tabid).click( function() { mytabber.activate( $(this).attr("data-id") ) } );
	    $('div#' + tab.tabid).addClass( "tab tab_" + tab.tabid )
	}
	//html += '</div>'
	//$(this.divid).prepend( html );
	if (name === undefined) {
	    name = this.tabs[0].tabid;
	}
	this.activate(name);
    };
    
};

