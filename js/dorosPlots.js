
function plotRawFBCT( fillno ) {
    var url = $(location).attr("href")
    //console.log ('url', url);
    var data = {'fillno' : fillno }
    //console.log( data );
    $('div#plot_early').html( "<h3>Loading plots... be patient</h3> ");
    $('div#plot_late').html( "" );
    $.post(url,data,function(reply){
	console.log(reply);
	$('div#message').html( reply.message );
	$('div#plot_early').html( reply.early );
	$('div#plot_late').html( reply.late );
    });
}

function plotRawDoros( fillno ) {
    var url = $(location).attr("href")
    //console.log ('url', url);
    var data = {'fillno' : fillno }
    //console.log( data );
    $('div#plot_lr').html( "<h3>Loading plots... be patient</h3> ");
    $('div#plot_avg').html( "" );
    $.post(url,data,function(reply){
	console.log(reply);
	$('div#message').html( reply.message );
	$('div#plot_lr').html( reply.lr );
	$('div#plot_avg').html( reply.avg );
    });
}

function plotMagnetCurrents( fillno ) {
    var url = $(location).attr("href")
    //console.log ('url', url);
    var data = {'fillno' : fillno }
    console.log( data );
    //$('div#currentplot').html( "<h3>Loading plots... be patient</h3> ");
    $('div#message').html( "<h3>Loading plots... be patient</h3> ");
    $('div#currentplot').html( "");
    $('div#currentplot_norm').html( "");
    $('div#currentplot_pict').html( "");
    
    $.post(url,data,function(reply){
	console.log(reply);
	$('div#message').html( reply.message );
	$('div#currentplot').html( reply.current );
	$('div#currentplot_norm').html( reply.current_norm );
	$('div#currentplot_pict').html( reply.picture );
    });
}
