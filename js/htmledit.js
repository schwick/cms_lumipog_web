var pagefile = "";
var head = "";

CKEDITOR.disableAutoInline = true;
var editor = undefined;

runEditor = function() {
    setTimeout( checkEditor, 3000 );
}

$('div#summarytext').keydown( function() {
    $('div#status').html( "modified" );
});

collectData = function( action ) {
    if (editor) {
        var text = editor.getData(false);
    } else {
        var text = "";
    }
    var record = { 'text' : text,
                   'pagefile' : pagefile,
                   'head' : head,
                   'action' : action};
    return record;
}

checkEditor = function() {
    if ( editor.checkDirty() ) {
        data = collectData( 'save' );
        if ( ! data ) {
            setTimeout( checkEditor, 3000 );
            return ;
        }
        var url = window.location.protocol + '//' + window.location.host;
        var path = window.location.pathname;
        path = path.substring(0,path.lastIndexOf('/'));
        url += path + '/pageHandler.py';        
	console.log( url );
        $.post( url, data, function( data ) {	    
            $('div#status').html(data.status);
	    if ('debug' in data) $('div#debug').html( data.debug );
        } );
        editor.resetDirty();
    }

    setTimeout( checkEditor, 3000 );

}

newSummary = function() {
    var url = $('input#url').val();
    editor = CKEDITOR.inline( 'summarytext');
            editor.on('key', function() {
                $('div#status').html("changed");
            });
    runEditor(  );
}

publishSummary = function() {
    data = { 'action' : 'publish' }
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/minutesHandler.py';
    $.post( url, data, function( data ) {
        //debug(data);
        $('div#status').html( data.status );
    })
    return
}
            
loadSummary = function() {
    data = { 'text' : "",
             'action' : 'load' };
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/minutesHandler.py';
    //debug("load from " + url );
    $.post( url, data, function( data ) {
        //debug( data );
        $('div#summarytext').html( data.text );
        $('input#date').val( data.date );
        $('input#suffix').val( data.suffix );
        $('input#indicourl').val( data.indicourl );
        $('input#shortdesc').val( data.shortdesc );
        $('textarea#purpose').val( data.purpose );
        editor = CKEDITOR.inline( 'summarytext');
        editor.resetDirty();
        editor.on('key', function() {
            $('div#status').html("changed");
        });
        
        runEditor( );
        $('div#status').html(data.status);
    } );
    
}

loadPrevious = function() {
    data = { 'text' : "",
             'action' : 'loadPrevious' };
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/minutesHandler.py';
    //debug("load from " + url );
    $.post( url, data, function( data ) {
        //debug( "suffix " +data.suffix );
        $('div#summarytext').html( data.text );
        $('input#date').val( data.date );
        $('input#suffix').val( data.suffix );
        $('input#indicourl').val( data.indicourl );
        $('input#shortdesc').val( data.shortdesc );
        $('textarea#purpose').val( data.purpose );
        editor = CKEDITOR.inline( 'summarytext');
        editor.resetDirty();
        editor.on('key', function() {
            $('div#status').html("changed");
        });
        
        runEditor( );
        $('div#status').html(data.status);
    } );
    
}

loadPage = function() {
    var page = $('select#page').val();
    if (page == ""){
        debug("empty");
        return;
    }
    $('input#action').val('loadpage');
    $('form#editPage').submit();
}

newPage = function() {
    var newPageName = $('input#newPageName').val();
    var patt = /^[^\s]+.html$/;
    console.log(newPageName);
    if (! patt.test( newPageName ) ) {
        error( "You must enter a valid page name (no whitespace characters, extension is \".html\")!" );
        return;
    }
    clerror();
    $('input#action').val('newPage');
    $('form#editPage').submit();
}

release = function( ) {
    // make sure the current editor data is saved
    var status = $('div#status').html();
    if ( status == "problem" )
        return;
    if (status == "changed" ) {
        myI = setTimeout( function() { release( "release" ); }, 1000 );
        warning("Waiting for summary to be saved...");
        return;
    }
    warning("");
    data = collectData( "release" );    
    var url = window.location.protocol + '//' + window.location.host;
    var path = window.location.pathname;
    path = path.substring(0,path.lastIndexOf('/'));
    url += path + '/pageHandler.py';
    $.post( url, data, function( data ) {
        //debug(data);
        $('div#status').html( data.status );
        if ( 'link' in data ) {
            $('td#devlink').html( '<a href="' + data.link + '" target="_blank">Link to final minutes in git-source/lpc-minutes</a>')
        } else {
            $('td#devlink').html('');
        }
        if ('error' in data) { error( data.error ); };
        if ('debug' in data) { debug( data.debug ); }
    })
    return
}
