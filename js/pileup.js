
pileupSubmit = function( atTime = false) {
    //console.log( "about to submit form", atTime)
    $('p#Waiter').show(500);
    $('div#pileup_download_cmssw').hide();
    $('div#pileup_download').hide();
    if ( atTime ) {
	$('input#atTime').val(1);
    } else {
	$('input#atTime').val(0);
    } 
    $.post('pileupDistribution.py', $('form#pileupform').serialize(), function( data )
	   {
	       //console.log("back");
	       //console.log( data );
	       var down_data = JSON.stringify(data.pileupdata );
	       var dataurl =`data:application:json', ${down_data}`;
	       $('a#downlink').attr('href',dataurl);
	       $('div#pileup_download').show(500);
	       $('p#Waiter').hide(500);
	       $('div#plot_pileup').html( data.pileupplot );
	       $('div#plot_beam').html( data.beamplot );
	       $('div#plot_btb').html( data.btbplot );
	       if ( $('input[name="poisson"]').is(':checked') == false ){
		   var down_data_cmssw = getCMSSWFunction( data.pileupdata );
		   //console.log( down_data_cmssw)
		   var dataurl_cmssw ='data:text/plain,'+ encodeURIComponent(down_data_cmssw);
		   $('a#downlink_cmssw').attr('href',dataurl_cmssw);
		   $('div#pileup_download_cmssw').show(500);
	       }
	   }
	  );
}

getCMSSWFunction = function( data ) {
    // Pileup values are corrected according to the CMSSW convention (they count as
    // pileup the "additional" interactions in the event.). The pileup 0 bin is
    // therefore not exported (events without interactions are not simulated, of course)
    func =  "# A distribution for Run3 studies (" + $('select[name="scenario"]').val() + ") derived from LHC simulations.\n";
    func += "# Colliding bunches: " + $('input[name="bunches"]').val() + "\n";
    func += "# Hadronic x-sec   : " + $('input[name="hadronic_xsec"]').val() + "mb" + "\n";
    func += "# Fill length      : " + $('input[name="fill_length"]').val() + "min" + "\n";
    func += "# Levelling time   : " + $('input[name="levelling_time"]').val() + "min" + "\n";
    func += "\n";
    func += "import FWCore.ParameterSet.Config as cms\n";
    func += "from SimGeneral.MixingModule.mix_probFunction_25ns_PoissonOOTPU_cfi import *\n";
    func += "mix.input.nbPileupEvents.probFunctionVariable = cms.vint32(\n\t";

    for (ix = 1; ix<data.pileup_values.length; ix++) {
	if ( (ix > 0 ) && ( ix%5 == 0 ) ) {
	    func += "\n\t";
	}
	func +=  data.pileup_values[ ix ]-1 + ", ";
    }
    func = func.substring(0,func.length-2);
    func += "    )\n\n";
    func += "mix.input.nbPileupEvents.probValue = cms.vdouble(\n\t";

    for (ix = 1; ix<data.fraction.length; ix++) {
	if ( (ix > 0) && (ix%5 == 0) ) {
	    func += "\n\t";
	}
	func += data.fraction[ ix ] + ", ";
    }
    func = func.substring(0,func.length-2);
    func += "    )\n\t";
    return func;
}
